package com.fomat.newsapp.`interface`

import com.fomat.newsapp.model.News
import retrofit2.Call
import retrofit2.http.GET

interface NewsService {
    @get:GET("v1/news")
    val sources: Call<News>
}