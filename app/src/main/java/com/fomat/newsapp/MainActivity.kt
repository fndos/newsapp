package com.fomat.newsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.fomat.newsapp.`interface`.NewsService
import com.fomat.newsapp.adapter.NewsAdapter
import com.fomat.newsapp.common.Common
import com.fomat.newsapp.model.News
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var layoutManager: LinearLayoutManager;
    lateinit var mService: NewsService;
    lateinit var adapter: NewsAdapter;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Init service
        mService = Common.newsService;

        // Init recycler view
        recycler_view_news.setHasFixedSize(true);
        layoutManager = LinearLayoutManager(this);
        recycler_view_news.layoutManager = layoutManager;
        loadNews();


    }

    private fun loadNews() {
        mService.sources.enqueue(object: retrofit2.Callback<News> {
            override fun onFailure(call: Call<News>, t: Throwable) {
                Toast.makeText(baseContext, "Failed", Toast.LENGTH_SHORT).show();
            }

            override fun onResponse(call: Call<News>, response: Response<News>) {
                println(response);
                adapter = NewsAdapter(response!!.body()!!);
                adapter.notifyDataSetChanged();
                recycler_view_news.adapter = adapter;
            }

        });
    }
}