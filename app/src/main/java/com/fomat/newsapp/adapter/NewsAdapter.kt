package com.fomat.newsapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.fomat.newsapp.R
import com.fomat.newsapp.model.NewsItem
import kotlinx.android.synthetic.main.news_layout.view.*

class NewsAdapter (private val dataList: MutableList<NewsItem>) : RecyclerView.Adapter<NewsHolder>() {

    private lateinit var context : Context;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        context = parent.context;
        return NewsHolder(LayoutInflater.from(context).inflate(R.layout.news_layout, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        val data = dataList[position]

        val newsTitleTextView = holder.itemView.news_title;
        val newsDescriptionTextView = holder.itemView.news_description;

        newsTitleTextView.text = data.title;
        newsDescriptionTextView.text = data.description;

        holder.itemView.setOnClickListener {
            Toast.makeText(context, data.title, Toast.LENGTH_SHORT).show();
        }
    }


}