package com.fomat.newsapp.common

import com.fomat.newsapp.`interface`.NewsService
import com.fomat.newsapp.remote.RetrofitClient


object Common {
    val BASE_URL = "https://6058933cc3f49200173ae6c7.mockapi.io/api/"

    val newsService : NewsService
        get() = RetrofitClient.getClient(BASE_URL).create(NewsService::class.java);
}