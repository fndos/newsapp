package com.fomat.newsapp.model


import com.google.gson.annotations.SerializedName

data class NewsItem(
    @SerializedName("author")
    val author: String,
    @SerializedName("createdAt")
    val createdAt: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("source")
    val source: String,
    @SerializedName("title")
    val title: String
)